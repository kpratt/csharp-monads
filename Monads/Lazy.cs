﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Monads
{
    public static class MetaLazy
    {
        public static Lazy<T> AsLazy<T>(this T t)
        {
            return new Lazy<T>(() => t);
        }
    }
    
    public class Lazy<T>
    {
        private int calcTriggered;
        private Option<T> value;
        private Func<T> f;
        
        public T Value { get {
            
            //do the calculation only once and make it thread safe
            if ( Interlocked.CompareExchange(ref calcTriggered, 1, 0) == 0 )
            {
                value = f().Some();
            }
            while (value.IsEmpty) Thread.Yield();
            return value.Get();
        } }

        public Lazy(Func<T> f)
        {
            this.f = f;
            this.value = new None<T>();
            this.calcTriggered = 0;
        }

        public Lazy<R> Select<R>(Func<T, R> f)
        {
            return new Lazy<R>(() => f(Value));
        }

        public Lazy<R> SelectMany<R>(Func<T, Lazy<R>> f)
        {
            return SelectMany<R, R>(f, (a, b) => b);
        }

        public Lazy<TResult> SelectMany<R, TResult>(Func<T, Lazy<R>> f, Func<T, R, TResult> resultSelector)
        {
            return new Lazy<TResult>(() => resultSelector(Value, f(Value).Value));
        }
    }
}
