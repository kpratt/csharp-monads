﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monads
{
    public static class MetaPromise
    {
        public static Promise<T> AsPromise<T>(this Func<T> f)
        {
            return new Promise<T>(f);
        }

        public static Promise<T> AsPromise<T>(this T t)
        {
            return new Promise<T>(() => t);
        }
    }

    public class Promise<T>
    {
        Task<T> task;
        
        public T Value { get {
            task.Wait();
            return task.Result;
        }}

        public Promise(Func<T> f)
        {
            task = Task.Factory.StartNew<T>(f);
        }

        private Promise(Task<T> t)
        {
            task = t;
        }

        public Promise<R> Select<R>(Func<T, R> f)
        {
            return new Promise<R>(task.ContinueWith<R>((Task<T> t) => f(t.Result)));
        }

        public Promise<R> SelectMany<R>(Func<T, Promise<R>> f)
        {
            return SelectMany<R, R>(f, (a, b) => b);
        }

        public Promise<TResult> SelectMany<R, TResult>(Func<T, Promise<R>> f, Func<T, R, TResult> resultSelector)
        {
            Promise<Promise<R>> p = new Promise<Promise<R>>(
                task.ContinueWith<Promise<R>>(
                    x => f(x.Result)
            ));
            return new Promise<TResult>(() => resultSelector(Value, p.Value.Value));
        }

        public Promise<T> Foreach(Action<T> f)
        {
            f(Value);
            return this;
        }
    }
}
