﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monads
{
    public static class MetaOption
    {
        public static Option<T> AsOption<T>(this T t)
        {
            if (t == null) return None<T>(); else return t.Some();
        }
        public static Option<T> None<T>()
        {
            return new None<T>();
        }

        public static Option<T> Some<T>(this T t)
        {
            return new Monads.Some<T>(t);
        }
    }

    public abstract class Option<T>
    {
        public abstract T Get();

        public abstract T GetOrElse(Func<T> f);

        public abstract Option<R> Select<R>(Func<T, R> f);
        public abstract Option<R> SelectMany<R>(Func<T, Option<R>> f);
        public abstract Option<TResult> SelectMany<R, TResult>(Func<T, Option<R>> f, Func<T, R, TResult> resultSelector);
        public abstract Option<T> Where(Func<T, bool> p);

        public abstract Option<T> Foreach(Action<T> f);

        public abstract bool IsEmpty { get; }
        public abstract bool IsDefined { get; }

    }

    public class Some<T> : Option<T>
    {
        private T Value { get; set; }

        public Some(T t)
        {
            Value = t;
        }

        public override T Get()
        {
            return Value;
        }

        public override T GetOrElse(Func<T> f)
        {
            return Value;
        }

        public override Option<R> Select<R>(Func<T, R> f)
        {
            return f(Value).Some();
        }

        public override Option<R> SelectMany<R>(Func<T, Option<R>> f)
        {
            return SelectMany(f, (a, b) => b);
        }

        public override Option<TResult> SelectMany<R, TResult>(Func<T, Option<R>> f, Func<T, R, TResult> resultSelector)
        {
            return f(Value).Select(X => resultSelector(Value, X));
        }

        public override Option<T> Where(Func<T, bool> p)
        {
            if (p(Value)) return this; else return MetaOption.None<T>();
        }

        public override Option<T> Foreach(Action<T> f)
        {
            f(Value);
            return this;
        }

        public override bool IsEmpty { get { return false; } }
        public override bool IsDefined { get { return true; } }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            var some = obj as Some<T>;
            return Value.Equals(some.Value);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }

    public class None<T> : Option<T>
    {


        public None() { }

        public override T Get()
        {
            throw new NullReferenceException();
        }

        public override T GetOrElse(Func<T> f)
        {
            return f();
        }

        public override Option<R> Select<R>(Func<T, R> f)
        {
            return new None<R>();
        }

        public override Option<R> SelectMany<R>(Func<T, Option<R>> f)
        {
            return new None<R>();
        }

        public override Option<TResult> SelectMany<R, TResult>(Func<T, Option<R>> f, Func<T, R, TResult> resultSelector)
        {
            return new None<TResult>();
        }

        public override Option<T> Where(Func<T, bool> p)
        {
            return this;
        }

        public override Option<T> Foreach(Action<T> f)
        {
            return this;
        }

        public override bool IsEmpty { get { return true; } }
        public override bool IsDefined { get { return false; } }

        public override bool Equals(object obj)
        {
            return typeof(None<T>).Equals(obj.GetType());
        }

        public override int GetHashCode()
        {
            return typeof(None<T>).GetHashCode();
        }
    }
}
