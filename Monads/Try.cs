﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monads
{
    public static class MetaTry {
        public static Try<R> Try<A, R>(this A a, Func<A, R> f)
        {
            try
            {
                return new Result<R>(f(a));
            }
            catch (Exception e)
            {
                return new Throw<R>(e);
            }
        }

        public static Try<R> Try<R>(Func<R> f)
        {
            try
            {
                return new Result<R>(f());
            }
            catch (Exception e)
            {
                return new Throw<R>(e);
            }
        }

        public static Try<R> AsTry<R>(this R r)
        {
            return new Result<R>(r);
        }
    }

    public abstract class Try<A>
    {
        public delegate R Func0<R>();
        public delegate R Func1<R>(A a);

        

        public abstract Try<R> Select<R>(Func<A, R> f);
        public abstract Try<R> SelectMany<R>(Func<A, Try<R>> f);
        public abstract Try<TResult> SelectMany<R, TResult>(Func<A, Try<R>> selector, Func<A, R, TResult> resultSelector);
        public abstract Try<A> Where(Func<A, Boolean> f);

        public abstract A Get();
        public abstract A GetOrElse(Func0<A> f);
        public abstract Try<A> Recover(Func<Exception, A> f);
        public abstract Try<A> Ensure(Action f);

        public abstract Boolean IsResult();
        public abstract Boolean IsThrow();

        public abstract Try<A> OnSuccess(Action<A> f);
        public abstract Try<A> OnFailure(Action<Exception> f);

    }

    public class Result<A> : Try<A>
    {

        A result;

        internal Result(A a)
        {
            result = a;
        }

        public override Try<R> Select<R>(Func<A, R> f)
        {
            try
            {
                return new Result<R>(f(result));
            }
            catch (Exception e)
            {
                return new Throw<R>(e);
            }
        }

        public override Try<R> SelectMany<R>(Func<A, Try<R>> f)
        {
            return SelectMany<R, R>(f, (a, b) => b);
        }
        
        public override Try<TResult> SelectMany<R, TResult>(Func<A, Try<R>> f, Func<A, R, TResult> resultSelector)
        {
            Try<R> gnu;
            try
            {
                gnu = f(result);
            }
            catch (Exception e)
            {
                gnu = new Throw<R>(e);
            }
            return gnu.Select(value => resultSelector(result, value));
        }

        public override Try<A> Where(Func<A, Boolean> f)
        {
            if (f(result))
                return this;
            else
                return new Throw<A>(new Exception("Where failed"));
        }

        public override A Get()
        {
            return result;
        }

        public override A GetOrElse(Func0<A> f)
        {
            return result;
        }

        public override Try<A> Recover(Func<Exception, A> f)
        {
            return this;
        }

        public override Try<A> Ensure(Action f)
        {
            f();
            return this;
        }

        public override Boolean IsResult() { return true; }
        public override Boolean IsThrow() { return false; }

        public override Try<A> OnSuccess(Action<A> f)
        {
            f(result);
            return this;
        }

        public override Try<A> OnFailure(Action<Exception> f)
        {
            return this;
        }

    }

    public class Throw<A> : Try<A>
    {
        public Exception thrown { get; private set; }

        internal Throw(Exception e)
        {
            thrown = e;
        }

        public override Try<R> Select<R>(Func<A, R> f)
        {
            return new Throw<R>(thrown);
        }

        public override Try<R> SelectMany<R>(Func<A, Try<R>> f)
        {
            return new Throw<R>(thrown);
        }

        public override Try<TResult> SelectMany<R, TResult>(Func<A, Try<R>> f, Func<A, R, TResult> resultSelector)
        {
            return new Throw<TResult>(thrown);
        }

        public override Try<A> Where(Func<A, Boolean> f)
        {
            return this;
        }

        public override A Get()
        {
            throw new Exception(thrown.Message, thrown);
        }

        public override A GetOrElse(Func0<A> f)
        {
            return f();
        }

        public override Try<A> Recover(Func<Exception, A> f)
        {
            return MetaTry.Try<A>(new Func<A>(() => f(thrown)));
        }

        public override Try<A> Ensure(Action f)
        {
            f();
            return this;
        }

        public override Boolean IsResult() { return false; }
        public override Boolean IsThrow() { return true; }

        public override Try<A> OnSuccess(Action<A> f)
        {
            return this;
        }

        public override Try<A> OnFailure(Action<Exception> f)
        {
            f(thrown);
            return this;
        }
    }

}
