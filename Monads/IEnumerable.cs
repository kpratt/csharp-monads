﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monads
{
    public static class IEnumerable
    {
        public static IEnumerable<R> Map<A, R>(this IEnumerable<A> thi, Func<A, R> f)
        {
            foreach (A a in thi)
            {
                yield return f(a);
            }
        }

        public static IEnumerable<R> Bind<A, R>(this IEnumerable<A> thi, Func<A, IEnumerable<R>> f) {
            foreach (A a in thi)
            {
                foreach (R r in f(a))
                {
                    yield return r;
                }
            }
        }

        public static IEnumerable<A> Filter<A>(this IEnumerable<A> thi, Func<A, bool> f)
        {
            foreach (A a in thi)
            {
                if (f(a))
                {
                    yield return a;
                }
            }
        }

        public static R Fold<A, R>(this IEnumerable<A> inum, R zero, Func<A, R, R> f)
        {
            if (inum.Count() == 0)
                return zero;
            else
                return f(inum.First(), Fold(inum, zero, f));
        }

        public static void Foreach<A>(this IEnumerable<A> inum, Action<A> f)
        {
            foreach (A a in inum) f(a);
        }

        public static Option<T> HeadOption<T>(this IEnumerable<T> inum)
        {
            if (inum.Count() > 0)
            {
                return inum.First().AsOption();
            }
            else
            {
                return new None<T>();
            }
        }
    }

}
