﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Monads;

namespace MonadTests
{
    [TestClass]
    public class LazyTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            var touched = new bool[] { false, false, false };
            var five = new Monads.Lazy<int>(() => 5);
            var six = five.Select(delegate(int f){ touched[0] = true; return f+1; });
            var seven = six.SelectMany<int>(delegate(int f) { touched[1] = true; return new Monads.Lazy<int>(() => f + 1); });

            Func<int, int> addThree = delegate(int v) { touched[2] = true; return v + 3; };

            var eight = from f in five
                        select addThree(f);

            AssertCollections.AreEqual(new bool[]{false, false, false}, touched);
            Assert.AreEqual(7, seven.Value);
            AssertCollections.AreEqual(new bool[] { true, true, false }, touched);
            Assert.AreEqual(8, eight.Value);
            AssertCollections.AreEqual(new bool[] { true, true, true }, touched);

        }
    }
}
