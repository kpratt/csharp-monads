﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Monads;

namespace MonadTests
{
    [TestClass]
    public class TryTest
    {
        [TestMethod]
        public void points()
        {
            Try<int> z = from x in 5.AsTry()
                    from y in 4.AsTry()
                    select x + y;

            Assert.AreEqual(9, z.Get());

            var hw = "Hello ".Try(s => s + "world!").Get();
            Assert.AreEqual("Hello world!", hw);
        }

        private int Double(int x) {
            throw new Exception("Hello world!");
        }

        [TestMethod]
        public void mapContainsException()
        {
            Func<int, int> f = Double;

            var hw = 5.AsTry().Select(f);
            Assert.AreEqual("Hello world!", (hw as Throw<int>).thrown.Message);
        }


    }
}
