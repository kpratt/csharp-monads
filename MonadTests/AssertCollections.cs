﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MonadTests
{
    public class AssertCollections
    {
        public static void AreEqual<T>(IEnumerable<T> expected, IEnumerable<T> actual){

            Assert.AreEqual(expected.Count(), actual.Count(), "Collections were of different size.");

            IEnumerator<T> eter = expected.GetEnumerator();
            IEnumerator<T> ater = actual.GetEnumerator();
            for (int i = 0; eter.MoveNext() && ater.MoveNext(); i++)
            {
                if (! eter.Current.Equals(ater.Current))
                {
                    Assert.Fail("Collections differ at index " + i);
                }
            }
        }

    }
}
