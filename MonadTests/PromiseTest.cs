﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Monads;
using System.Threading;

namespace MonadTests
{
    [TestClass]
    public class PromiseTest
    {

        private int sleepyIncrement(int x)
        {
            Thread.Sleep(1000);
            return x + 1;
        }

        [TestMethod]
        public void TestMethod1()
        {
            var start = DateTime.Now;
            Promise<int> origin = 5.AsPromise<int>();
            Promise<int> conclusion = origin.
                Select<int>(sleepyIncrement).
                Select<int>(sleepyIncrement).
                Select<int>(sleepyIncrement);

            var composed = DateTime.Now;
            Assert.AreEqual(8, conclusion.Value);
            var asserting = DateTime.Now;

            var compTime = composed.Subtract(start);
            if (compTime.TotalMilliseconds > 10)
                Assert.Fail("took to long [" + compTime.TotalMilliseconds + "]");

            var assertTime = asserting.Subtract(start);
            if (assertTime.TotalMilliseconds > 3010)
                Assert.Fail("took far to long [ " + assertTime.TotalMilliseconds + " ]");
        }

        [TestMethod]
        public void TestMethod2()
        {
            var start = DateTime.Now;
            var q = from x in 4.AsPromise().Select(sleepyIncrement)
                    from y in 4.AsPromise().Select(sleepyIncrement)
                    from z in 4.AsPromise().Select(sleepyIncrement)
                    select x + y + z;
            
            var composed = DateTime.Now;
            Assert.AreEqual(15, q.Value);
            var asserting = DateTime.Now;

            var compTime = composed.Subtract(start);
            if (compTime.TotalMilliseconds > 10)
                Assert.Fail("took to long [" + compTime.TotalMilliseconds + "]");

            var assertTime = asserting.Subtract(start);
            if (assertTime.TotalMilliseconds > 3010)
                Assert.Fail("took far to long [ " + assertTime.TotalMilliseconds + " ]");

        }

        [TestMethod]
        public void TestMethod3()
        {
            var start = DateTime.Now;
            var X = 4.AsPromise().Select(sleepyIncrement);
            var Y = 4.AsPromise().Select(sleepyIncrement);
            var Z = 4.AsPromise().Select(sleepyIncrement);

            var q = from x in X
                    from y in Y
                    from z in Z
                    select x + y + z;

            var composed = DateTime.Now;
            Assert.AreEqual(15, q.Value);
            var asserting = DateTime.Now;

            var compTime = composed.Subtract(start);
            if (compTime.TotalMilliseconds > 10)
                Assert.Fail("took to long [" + compTime.TotalMilliseconds + "]");

            var assertTime = asserting.Subtract(start);
            if (assertTime.TotalMilliseconds > 1010)
                Assert.Fail("took far to long [ " + assertTime.TotalMilliseconds + " ]");

        }
    }
}
