﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Monads;

namespace MonadTests
{
    [TestClass]
    public class OptionTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            Option<int> sumOf3 = from x in 3.AsOption()
                                 from y in 4.AsOption()
                                 from z in new None<int>()
                                 select x + y + z;
            Assert.AreEqual(new None<int>(), sumOf3);
        }

        [TestMethod]
        public void TestMethod2()
        {
            Option<int> sumOf3 = from x in 3.AsOption()
                                 from y in 4.AsOption()
                                 from z in 5.AsOption()
                                 select x + y + z;

            Assert.AreEqual(12.Some(), sumOf3);
        }
    }
}
